var zmq = require("zeromq");
var assert = require('assert');
var msgpack = require('msgpack');
const axios = require('axios');
const pg = require('pg');
var qs = require('qs');
const { v4: uuidv4 } = require('uuid');

const postgres_config = {
    user: process.env.DATABASE_USERNAME,
    host: process.env.DATABASE_HOST,
    database: 'shepdjango',
    password: process.env.DATABASE_PASSWORD,
    port: '5432'
}

async function checkRowExists(pool, table, ip_address, board_type) {
    const query_text = `SELECT EXISTS(SELECT 1 FROM "${table}" WHERE ip_address='${ip_address}' AND board_type='${board_type}');`;
    var response = await pool.query(query_text);
    return response.rows[0]["exists"];
}

module.exports = {
    boardListener: async function() {
        const sock = new zmq.Reply;
        await sock.bind("tcp://*:6001");
        var shep = process.env.SHEP_HOSTNAME;
        var instance = axios.create({
            baseURL: `http://${shep}/`,
            timeout: 10000,
        });
        console.log("Listening for board registrations on port 6001.");
        for await (const [msg] of sock) {
            var [uuid, address, board_type, devices] = msgpack.unpack(msg);
            data = {
                uuid: uuid,
                ip_address: address,
                board_type: board_type
            };
            console.log(data);
            try {
                response = await instance.post(url='api/boards/', data=qs.stringify(data));
                sock.send(msgpack.pack(["ok"]));
                console.log(response.data);
            } catch (error) {
                if (error.response.status == 400 && error.response.data[0] == "Board already exists") {
                    sock.send(msgpack.pack(["ok"]));
                    console.log("Board already exists, so sending ok packet.")
                } else {
                    console.log(error);
                }
            }
        }
    }
}
