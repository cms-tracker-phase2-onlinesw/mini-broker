#!/bin/sh

while [ "$(curl -sL -w "%{http_code}" -I http://$SHEP_HOSTNAME/api/ -o /dev/null)" != 200 ]; do echo "Shep API not responding, waiting..."; sleep 2; done

node server.js
