var ping = require('ping');
const pg = require('pg');

const postgres_config = {
    user: process.env.DATABASE_USERNAME,
    host: process.env.DATABASE_HOST,
    database: 'shepdjango',
    password: process.env.DATABASE_PASSWORD,
    port: '5432'
}

function setOffline(pool, table, ip_address) {
    const query_text = `UPDATE "${table}" SET "online" = '0' WHERE ("ip_address" = '${ip_address}' AND "online" = '1');`;
    pool.query(query_text, (err, response) => {
        if (process.env.DEBUG == 1) {
            console.log(err ? err.stack : "Board "+ ip_address +" set to be offline.");
        };
    })
}

function setOnline(pool, table, ip_address) {
    const query_text = `UPDATE "${table}" SET "online" = '1' WHERE ("ip_address" = '${ip_address}' AND "online" = '0');`;
    pool.query(query_text, (err, response) => {
        if (process.env.DEBUG == 1) {
            console.log(err ? err.stack : "Board "+ ip_address +" set to be online.");
        };
    })
}


module.exports = {
    listenLoop: function () {
        const pool = new pg.Pool(postgres_config);
        const table = "api_board";
        const query_text = `SELECT ip_address FROM "${table}";`;
        pool.query(query_text, (err, response) => {
            hosts = [];
            try {
                response.rows.forEach(function (row) {
                    hosts.push(row.ip_address);
                });
            } catch {};
            if (hosts.length > 0) {
                hosts.forEach(function(host){
                    ping.sys.probe(host, function(isAlive){
                        if (isAlive) {
                            // var msg = 'Heartbeat | Board at ' + host + ' is up.';
                            // console.log(msg);
                            setOnline(pool, table, host);
                        } else {
                            if (process.env.DEBUG == 1) {
                                var msg = 'Heartbeat | Board at ' + host + ' is down, marking as offline...';
                                console.log(msg);
                            };
                            setOffline(pool, table, host);
                        }
                    });
                });
            }
        });
    }
}
