const http = require('http');
var zmq = require("zeromq");
var assert = require('assert');
var msgpack = require('msgpack');

async function sendDataToBoards(addresses, port, data, timeout) {
    var reply;
    var packets = [];
    for (a of addresses) {
        packets.push([a, port, data, timeout]);
    }
    /* Send command data to all addresses specified. Each request is
       asynchronous but all must finish before the call completes. */
    await Promise.all(packets.map(async function(packet) {
        var [address, port, data, timeout] = packet;
        const sock = new zmq.Request;
        sock.connect("tcp://" + address + ":" + port);
        console.log("Connecting to " + address + " at port " + port);
        // Set timout to ensure the Promise does not hang indefinitely.
        sock.receiveTimeout = parseInt(timeout);
        await sock.send(data);
        const [result] = await sock.receive();
        return result;
    })).catch(e => {
        reply = [e.toString()];
        return reply;
    }).then(allData => {
        reply = {};
        for (i in addresses) {
            reply[addresses[i]] = allData[i];
        }
    });
    return reply;
};


async function commandLoop() {
    const sock = new zmq.Reply;
    await sock.bind("tcp://*:1234");
    console.log("Listening for commands on port 1234.");
    for await (const [msg] of sock) {
        var [addresses, data, timeout] = msgpack.unpack(msg);
        var reply = await sendDataToBoards(addresses, "12345", msgpack.pack(data), timeout);
        sock.send(msgpack.pack(reply));
    }
};


async function restServer() {
    const server = http.createServer();
    server.on('request', (request, response) => {
        const { method, url, headers } = request;
        if (method === 'POST' && url === '/api/request') {
            const {config} = headers;
            const {addresses, timeout} = JSON.parse(config);
            console.log(addresses);
            console.log(timeout);
            console.log(url);
            console.log(method);
            let body = [];
            request.on('data', (chunk) => {
                body.push(chunk);
            }).on('end', () => {
                data = Buffer.concat(body);
                sendDataToBoards(addresses, "12345", data, timeout).then(reply => {
                    console.log(reply);
                    response.setHeader('Content-Type', 'application/json');
                    response.end(JSON.stringify(reply));
                });
            });
        } else {
            response.statusCode = 404;
            response.end();
        }
    }).listen(80);
}

restServer();
