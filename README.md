# Mini-broker
A simplified version of the server-side broker used to communicate between shep and HERD instances.

## Operation
The broker has two main purposes:
- Receive HTTP requests fom shep instances and route the data in these requests out to the specified HERD instance. Any data which is then sent back from the HERD isntance is passed along to the shep instance.
- Register HERD instances and check the health of the instances.

## Ports
| Port | Usage |
| ---  | ---   |
| 80   | Used to access the mini-broker's RESTful API. |
| 6001 | Used to receive requests for board registration. |

## API reference
- `/api/request`: This the end point used to send requests out to the boards managed by the broker. The header must include a JSON string labelled `config`, within which a list of boards and request timeout must be provided. The data for the request is arbitrary and will be sent to the boards untouched. In the case of [herd-control-app](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/herd-control-app) applications, this data must be of [msgpack](https://msgpack.org/index.html) format. Example config JSON:
```json
{
    "addresses": ["192.168.18.1", "192.168.18.2"],
    "timeout": 10000
}
```

## Docker Container
### Supported Architectures
Currently the mini-broker image can only run on `x86_64` machines.
### Usage
Here are some example snippets to help you get started creating a container.
#### docker
```
docker create \
    --name=broker \
    --restart on-failure \
    -p 6001:6001 \
    -p 80 `# Only required if not in the same stack as a shep instance` \
    gitlab-registry.cern.ch/cms-tracker-phase2-onlinesw/stack-test/mini-broker
```
#### docker-compose
```
---
version: "3"

services:
    broker:
        image: gitlab-registry.cern.ch/cms-tracker-phase2-onlinesw/stack-test/mini-broker
        container_name: broker
        restart: on-failure
        ports:
            - 6001:6001
            - 80
```
