var zmq = require("zeromq");
var msgpack = require('msgpack');
var command = require('./command');
var boardregistration = require('./board-registration');
var heartbeat = require('./heartbeat');

command.restServer();
boardregistration.boardListener();

setInterval(function () {
    heartbeat.listenLoop();
}, 10000);
